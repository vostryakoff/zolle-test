import slick from "slick-carousel";
import checkForm from "./checkForm";
import selectize from "selectize";

export default {
    init: () => {

    	let _this = this,
    		$slider = $('.js-cases');
    	function zero_for_number(num) {
    		return num < 10 ? '0' + num : num;
    	}
        $slider.on('init', (event, slick, currentSlide, nextSlide) => {
			$('.counts-all').text(zero_for_number(slick.slideCount));
		}).on('beforeChange', (event, slick, currentSlide, nextSlide) => {
		    $('.count-cur').text(zero_for_number(nextSlide + 1));

		});
        $slider.slick({
        	fade:true,
        	dots: true,
        	slidesToShow: 1,
        	prevArrow:$('.cases__prev'),
            nextArrow: $('.cases__next'),
            appendDots: $('.cases__dots'),
        	adaptiveHeight: true
        });

        let $steps = $('.js-steps'),
        	$progressbar_all = $('.step__progressbar-all'),
        	$progressbar_current = $('.step__progressbar-current');
        $steps.on('init', (event, slick, currentSlide, nextSlide) => {
			$progressbar_all.text(slick.slideCount);
			$progressbar_current.css('width', Math.ceil(100 / slick.slideCount) + '%');
		})
		$steps.on('beforeChange', (event, slick, currentSlide, nextSlide) => {
		    $('.step__progressbar-from').text(nextSlide + 1);
		    $progressbar_current.css('width', Math.ceil(((nextSlide + 1) * 100) / slick.slideCount) + '%');
		});
        $steps.slick({
        	slidesToShow: 1,
        	arrows: false,
        	infinite:false,
        	touchMove: false,
        	adaptiveHeight: true,
        	swipe: false,
        	touchMove: false
        	// fade:true
        });

        $('.js-select').selectize({
        	dropdownParent: 'body'
        });
        $('.js-digits').keydown(function (event) {
			if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
				(event.keyCode == 65 && event.ctrlKey === true) ||
				(event.keyCode >= 35 && event.keyCode <= 39)) {
				return;
			} else {
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
					event.preventDefault();
				}
			}
		});
        $('.js-btn-next').on('click', function(e) {
        	e.preventDefault();
        	if (checkForm.check($(this).closest('.form-content'))) {
        		$steps.slick('slickNext');
        	}
        });
        $('.js-submit').on('click', function(event) {
        	event.preventDefault();
        	if (checkForm.check($(this).closest('.form-content'))) {
        		$('.form-section__inner').html($('.success-content').html());
        	}
        });
        $('body').on('click','.js-close-popup', (event) => {
        	event.preventDefault();
        	$('.form-section').fadeOut();
        });
    }
}
