export default {
    check: (step) => {
		let checkResult = true;

		// Браузер кеширует состояние, повторное нажатие не делает анимацию ошибки. 
		$('.warning').removeClass('warning').each(function() {
			// принудительно вызываем reflow 
			this.offsetHeight;
		});
				
		step.find('input, textarea, select').each((i, el) => {
			let $el = $(el),
				$el_field = $el.closest('.field').length ? $el.closest('.field') : $el;
			if ($el.data('req')) {
				switch ($el.data('type')) {
				case 'tel':
					var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
					if (!re.test($el.val())) {
						$el_field.addClass('warning');
						checkResult = false;
					}
					break;
				case 'email':
					var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					if (!re.test($el.val())) {
						$el_field.addClass('warning');
						checkResult = false;
					}
					break;
				case 'time':
					var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/i;
					if (!re.test($el.val())) {
						$el_field.addClass('warning');
						checkResult = false;
					}
				default:
					if ($.trim($el.val()) === '') {
						$el_field.addClass('warning');
						checkResult = false;
					}
					break;
				}
			}
		});

		if (!checkResult)
			step.addClass('warning');

		return checkResult;
    }
}